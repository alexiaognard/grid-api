#!/usr/bin/env python

""" Utils """


def check_win(grid, mark):
    """
    Check if the current user just win

    :param grid: grid data
    :type grid: list
    :param mark: mark of current player
    :type mark: char
    :return is_win: is the player win
    :rtype is_win: boolean
    """

    # 0 1 2
    # 3 4 5
    # 6 7 8


    return (grid[0] == grid[1] == grid[2] == mark) or \
           (grid[3] == grid[4] == grid[5] == mark) or \
           (grid[6] == grid[7] == grid[8] == mark) or \
           (grid[0] == grid[3] == grid[6] == mark) or \
           (grid[1] == grid[4] == grid[7] == mark) or \
           (grid[2] == grid[5] == grid[8] == mark) or \
           (grid[0] == grid[4] == grid[8] == mark) or \
           (grid[2] == grid[4] == grid[6] == mark)
